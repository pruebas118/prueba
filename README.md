servicio REST que me permite consultar la información básica de un cliente.

GET para consulta:
localhost:8090/prueba/cliente/buscar?tipoDocumento=C&numeroDocumento=23445322

POST para crear nuevo cliente:
localhost:8090/prueba/cliente
Parametros:
{
"tipoDocumento":"C",
"numeroDocumento":"1234567",
"primerNombre":"Nombre1",
"segundoNombre":"Nombre2",
"primerApellido":"Apellido1",
"segundoApellido":"Apellido2"
}
