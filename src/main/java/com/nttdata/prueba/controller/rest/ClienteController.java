package com.nttdata.prueba.controller.rest;

import com.nttdata.prueba.common.util.ClienteLogger;
import com.nttdata.prueba.model.dto.ClienteDTO;
import com.nttdata.prueba.model.dto.request.ClienteRequest;
import com.nttdata.prueba.model.dto.response.ClienteResponse;
import com.nttdata.prueba.model.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("cliente")
public class ClienteController {
    private final ClienteService clienteService;
    private final ClienteLogger clienteLogger;
    
    @Autowired
    public ClienteController(ClienteService clienteService, ClienteLogger clienteLogger) {
        this.clienteService = clienteService;
        this.clienteLogger = clienteLogger;
    }
    
    @GetMapping
    public Iterable<ClienteResponse> obtenerTodosLosClientes() {
        clienteLogger.logInfo("Consultando listado de todos los clientes");
        return clienteService.obtenerTodosLosClientes();
    }
    
    @GetMapping("/buscar")
    public Iterable<ClienteResponse> buscarClientesPorDocumento(
            @RequestParam String tipoDocumento,
            @RequestParam String numeroDocumento
    ) {
        clienteLogger.logInfo("Consultando cliente con tipoDocumento: " 
                + tipoDocumento + " numeroDocumento: " + numeroDocumento);
        return clienteService.buscarClientesPorDocumento(tipoDocumento, numeroDocumento);
    }
    
    @GetMapping("/{id}")
    public ClienteDTO obtenerClientePorId(@PathVariable Long id) {
        clienteLogger.logInfo("Consultando cliente con ID: " + id);
        return clienteService.obtenerClientePorId(id);
    }

    @PostMapping
    public ClienteResponse crearCliente(@RequestBody ClienteRequest clienteRequest) {
        clienteLogger.logInfo("Creando nuevo cliente");
        return clienteService.crearCliente(clienteRequest);
    }

    @PutMapping("/{id}")
    public ClienteResponse actualizarCliente(@PathVariable Long id, @RequestBody ClienteRequest request) {
        clienteLogger.logInfo("Actualizando cliente con ID: " + id);
        return clienteService.actualizarCliente(id, request);
    }

    @DeleteMapping("/{id}")
    public void eliminarCliente(@PathVariable Long id) {
        clienteLogger.logInfo("Eliminando cliente con ID: " + id);
        clienteService.eliminarCliente(id);
    }
}

