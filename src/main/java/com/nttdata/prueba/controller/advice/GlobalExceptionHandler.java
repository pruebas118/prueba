package com.nttdata.prueba.controller.advice;

import com.nttdata.prueba.common.exception.ClienteException;
import com.nttdata.prueba.common.util.ApiError;
import com.nttdata.prueba.common.util.ClienteLogger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    private final ClienteLogger clienteLogger = new ClienteLogger();
    
    @ExceptionHandler(ClienteException.class)
    public ResponseEntity<ApiError> handleEmptyInput(ClienteException ex){
        ApiError apiError = new ApiError(ex.getReason());
        clienteLogger.logError("Error no controlado: " + ex.getMessage());
        return new ResponseEntity<>(apiError, ex.getStatus());
    }
    
}
