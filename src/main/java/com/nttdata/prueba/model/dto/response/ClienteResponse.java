package com.nttdata.prueba.model.dto.response;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class ClienteResponse {
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String telefono;
    private String direccion;
    private String ciudadResidencia;
}
