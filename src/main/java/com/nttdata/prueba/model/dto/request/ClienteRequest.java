package com.nttdata.prueba.model.dto.request;

import com.nttdata.prueba.model.enums.TipoDocumento;
import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class ClienteRequest {
    private TipoDocumento tipoDocumento;
    private String numeroDocumento;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String telefono;
    private String direccion;
    private String ciudadResidencia;
}
