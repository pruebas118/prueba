package com.nttdata.prueba.model.entity;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cliente")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Size(max = 1, message = "El tipo de documento no es válido")
    @NotNull(message = "El tipo de documento no puede ser nulo")
    @Pattern(regexp = "^[CP]$", message = "El tipo de documento debe ser 'C' o 'P'")
    private String tipoDocumento;

    @NotNull(message = "El número de documento no puede ser nulo")
    @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "El número de documento debe contener solo caracteres alfanuméricos")
    private String numeroDocumento;

    @NotNull(message = "El primer nombre no puede ser nulo")
    private String primerNombre;

    private String segundoNombre;

    @NotNull(message = "El primer apellido no puede ser nulo")
    private String primerApellido;

    private String segundoApellido;
    private String telefono;
    private String direccion;
    private String ciudadResidencia;
    
}