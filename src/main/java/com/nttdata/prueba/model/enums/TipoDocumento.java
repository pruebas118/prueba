package com.nttdata.prueba.model.enums;

public enum TipoDocumento {
    C("Cédula de ciudadanía"),
    P("Pasaporte");

    private final String descripcion;

    TipoDocumento(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
