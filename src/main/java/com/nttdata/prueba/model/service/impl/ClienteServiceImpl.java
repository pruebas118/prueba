package com.nttdata.prueba.model.service.impl;

import com.nttdata.prueba.common.exception.ClienteException;
import com.nttdata.prueba.common.util.ClienteConstant;
import com.nttdata.prueba.common.util.ClienteLogger;
import com.nttdata.prueba.model.dto.ClienteDTO;
import com.nttdata.prueba.model.dto.request.ClienteRequest;
import com.nttdata.prueba.model.dto.response.ClienteResponse;
import com.nttdata.prueba.model.entity.Cliente;
import com.nttdata.prueba.model.mapper.ClienteMapper;
import com.nttdata.prueba.model.repository.ClienteRepository;
import com.nttdata.prueba.model.service.ClienteService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl implements ClienteService {

    private final ClienteRepository clienteRepository;
    private final ClienteMapper clienteMapper;
    private final ClienteLogger clienteLogger;

    @Autowired
    public ClienteServiceImpl(
            ClienteRepository clienteRepository,
            ClienteMapper clienteMapper,
            ClienteLogger clienteLogger
    ) {
        this.clienteRepository = clienteRepository;
        this.clienteMapper = clienteMapper;
        this.clienteLogger = clienteLogger;
    }

    @Override
    public ClienteResponse crearCliente(ClienteRequest clienteRequest) {
        Cliente cliente = clienteMapper.toClienteEntity(clienteRequest);
        cliente = clienteRepository.save(cliente);
        clienteLogger.logInfo("Cliente creado exitosamente.");
        return clienteMapper.toClienteResponse(cliente);
    }

    @Override
    public ClienteResponse actualizarCliente(Long clienteId, ClienteRequest clienteRequest) {
        Cliente clienteExistente = clienteRepository.findById(clienteId)
                .orElseThrow(() -> new ClienteException(
                HttpStatus.NOT_FOUND, //404 
                String.format(ClienteConstant.CLIENTE_NO_EXISTE_MESSAGE_ERROR,
                        clienteRequest.getTipoDocumento(),
                        clienteRequest.getNumeroDocumento())
        ));

        clienteExistente = clienteRepository.save(clienteExistente);
        clienteLogger.logInfo("Cliente actualizado exitosamente.");
        return clienteMapper.toClienteResponse(clienteExistente);
    }

    @Override
    public void eliminarCliente(Long clienteId) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(clienteId);
        if (clienteOptional.isPresent()) {
            clienteRepository.deleteById(clienteId);
            clienteLogger.logInfo("Cliente eliminado exitosamente.");
        } else {
            throw new ClienteException(HttpStatus.NOT_FOUND, "Cliente con ID " + clienteId + " no encontrado. No se pudo eliminar.");
        }
        clienteLogger.logInfo("Cliente eliminado exitosamente.");
    }

    @Override
    public ClienteDTO obtenerClientePorId(Long clienteId) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(clienteId);
        if (clienteOptional.isPresent()) {
            Cliente cliente = clienteOptional.get();
            return clienteMapper.toClienteDTO(cliente);
        } else {
            throw new ClienteException(HttpStatus.NOT_FOUND, "Cliente con ID " + clienteId + " no encontrado.");
        }
    }

    @Override
    public Iterable<ClienteResponse> buscarClientesPorDocumento(String tipoDocumento, String numeroDocumento) {
        if (tipoDocumento == null || tipoDocumento.isEmpty()) {
            throw new ClienteException(HttpStatus.BAD_REQUEST, "El tipo de documento es nulo o vacío");
        }

        if (numeroDocumento == null || numeroDocumento.isEmpty()) {
            throw new ClienteException(HttpStatus.BAD_REQUEST, "El número de documento es nulo o vacío");
        }
        Iterable<Cliente> clientes = clienteRepository.findByTipoDocumentoAndNumeroDocumento(tipoDocumento, numeroDocumento);

        if (clientes == null || !clientes.iterator().hasNext()) {
            throw new ClienteException(HttpStatus.NOT_FOUND,
                    String.format(ClienteConstant.CLIENTE_NO_EXISTE_MESSAGE_ERROR,
                            tipoDocumento, numeroDocumento));
        }

        List<ClienteResponse> clienteResponses = new ArrayList<>();

        for (Cliente cliente : clientes) {
            clienteResponses.add(clienteMapper.toClienteResponse(cliente));
        }

        return clienteResponses;
    }

    @Override
    public Iterable<ClienteResponse> obtenerTodosLosClientes() {
        Iterable<Cliente> clientes = clienteRepository.findAll();
        List<ClienteResponse> clienteResponses = new ArrayList<>();

        for (Cliente cliente : clientes) {
            clienteResponses.add(clienteMapper.toClienteResponse(cliente));
        }

        return clienteResponses;
    }
}
