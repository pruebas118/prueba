package com.nttdata.prueba.model.service.inicializacion;

import com.nttdata.prueba.model.entity.Cliente;
import com.nttdata.prueba.model.enums.TipoDocumento;
import com.nttdata.prueba.model.repository.ClienteRepository;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ClienteInicializacionService {
    private final ClienteRepository clienteRepository;

    @Autowired
    public ClienteInicializacionService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @PostConstruct
    public void init() {
        Cliente cliente1 = new Cliente();
        cliente1.setTipoDocumento(TipoDocumento.C.name());
        cliente1.setNumeroDocumento("23445322");
        cliente1.setPrimerNombre("Luis");
        cliente1.setSegundoNombre("Jose");
        cliente1.setPrimerApellido("Gomez");
        cliente1.setSegundoApellido("Perez");
        cliente1.setTelefono("3211567231");
        cliente1.setDireccion("Calle 1 # 2 - 3");
        cliente1.setCiudadResidencia("Bogotá D.C.");

        Cliente cliente2 = new Cliente();
        cliente2.setTipoDocumento(TipoDocumento.P.name());
        cliente2.setNumeroDocumento("AB123456");
        cliente2.setPrimerNombre("Sara");
        cliente2.setPrimerApellido("Vega");

        clienteRepository.save(cliente1);
        clienteRepository.save(cliente2);
    }
}

