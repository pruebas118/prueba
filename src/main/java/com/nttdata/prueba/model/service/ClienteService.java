package com.nttdata.prueba.model.service;

import com.nttdata.prueba.model.dto.ClienteDTO;
import com.nttdata.prueba.model.dto.request.ClienteRequest;
import com.nttdata.prueba.model.dto.response.ClienteResponse;


public interface ClienteService {
    ClienteResponse crearCliente(ClienteRequest clienteRequest);
    ClienteResponse actualizarCliente(Long clienteId, ClienteRequest clienteRequest);
    void eliminarCliente(Long clienteId);
    ClienteDTO obtenerClientePorId(Long clienteId);
    Iterable<ClienteResponse> buscarClientesPorDocumento(String tipoDocumento, String numeroDocumento);
    Iterable<ClienteResponse> obtenerTodosLosClientes();
}