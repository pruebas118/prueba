package com.nttdata.prueba.model.mapper;

import com.nttdata.prueba.model.dto.ClienteDTO;
import com.nttdata.prueba.model.dto.request.ClienteRequest;
import com.nttdata.prueba.model.dto.response.ClienteResponse;
import com.nttdata.prueba.model.entity.Cliente;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ClienteMapper {
    
    //toClienteEntity
    Cliente toClienteEntity(ClienteDTO clienteDTO);
    
    Cliente toClienteEntity(ClienteResponse response);
    
    @Mapping(source = "tipoDocumento", target = "tipoDocumento")
    @Mapping(source = "numeroDocumento", target = "numeroDocumento")
    @Mapping(source = "primerNombre", target = "primerNombre")
    @Mapping(source = "segundoNombre", target = "segundoNombre")
    @Mapping(source = "primerApellido", target = "primerApellido")
    @Mapping(source = "segundoApellido", target = "segundoApellido")
    @Mapping(source = "telefono", target = "telefono")
    @Mapping(source = "direccion", target = "direccion")
    @Mapping(source = "ciudadResidencia", target = "ciudadResidencia")
    Cliente toClienteEntity(ClienteRequest request);
    
    
    //fromClienteEntity
    @Mapping(source = "id", target = "id")
    @Mapping(source = "tipoDocumento", target = "tipoDocumento")
    @Mapping(source = "numeroDocumento", target = "numeroDocumento")
    @Mapping(source = "primerNombre", target = "primerNombre")
    @Mapping(source = "segundoNombre", target = "segundoNombre")
    @Mapping(source = "primerApellido", target = "primerApellido")
    @Mapping(source = "segundoApellido", target = "segundoApellido")
    @Mapping(source = "telefono", target = "telefono")
    @Mapping(source = "direccion", target = "direccion")
    @Mapping(source = "ciudadResidencia", target = "ciudadResidencia")
    ClienteDTO toClienteDTO(Cliente cliente);
    
    @Mapping(source = "primerNombre", target = "primerNombre")
    @Mapping(source = "segundoNombre", target = "segundoNombre")
    @Mapping(source = "primerApellido", target = "primerApellido")
    @Mapping(source = "segundoApellido", target = "segundoApellido")
    @Mapping(source = "telefono", target = "telefono")
    @Mapping(source = "direccion", target = "direccion")
    @Mapping(source = "ciudadResidencia", target = "ciudadResidencia")
    ClienteResponse toClienteResponse(Cliente cliente);
    
    ClienteRequest toClienteRequest(Cliente cliente);

}
