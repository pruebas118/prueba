package com.nttdata.prueba.common.exception;

import com.nttdata.prueba.common.util.ClienteLogger;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;


public class ClienteException extends ResponseStatusException  {
    private final ClienteLogger clienteLogger = new ClienteLogger();
    
    public ClienteException(HttpStatus status) {
        super(status);
        clienteLogger.logError("Excepción de cliente con status: " + status);
    }

    public ClienteException(HttpStatus status, String reason) {
        super(status, reason);
        clienteLogger.logError("Excepción de cliente: " + reason + " con status: " + status);
    }

    public ClienteException(HttpStatus status, String reason, Throwable cause) {
        super(status, reason, cause);
        clienteLogger.logError("Excepción de cliente: " + reason + " con status: " + status, cause);
    }
}