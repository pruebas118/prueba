package com.nttdata.prueba.common.util;


public class ClienteConstant {
    public static final String CLIENTE_NO_EXISTE_MESSAGE_ERROR = 
            "No se encontró un cliente con identificación: %s %s";
}
