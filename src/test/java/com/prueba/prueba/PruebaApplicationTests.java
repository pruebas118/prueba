package com.prueba.prueba;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.prueba.PruebaApplication;
import com.nttdata.prueba.controller.rest.ClienteController;
import com.nttdata.prueba.model.dto.request.ClienteRequest;
import com.nttdata.prueba.model.enums.TipoDocumento;
import com.nttdata.prueba.model.service.ClienteService;
import java.text.SimpleDateFormat;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = PruebaApplication.class)
@AutoConfigureMockMvc
class PruebaApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteController clienteController;

    @Test
    public void testBuscarCliente() throws Exception {
        String tipoDocumento = TipoDocumento.C.name();
        String numeroDocumento = "23445322";

        ResultActions resultActionsGet = mockMvc.perform(MockMvcRequestBuilders
                .get("/cliente/buscar")
                .param("tipoDocumento", tipoDocumento)
                .param("numeroDocumento", numeroDocumento)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()); //200

        String responseContent = resultActionsGet.andReturn().getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(responseContent);

        assertTrue(jsonNode.get(0).has("primerNombre"));
        assertTrue(jsonNode.get(0).has("segundoNombre"));
        assertTrue(jsonNode.get(0).has("primerApellido"));
        assertTrue(jsonNode.get(0).has("segundoApellido"));
        assertTrue(jsonNode.get(0).has("telefono"));
        assertTrue(jsonNode.get(0).has("direccion"));
        assertTrue(jsonNode.get(0).has("ciudadResidencia"));

    }

    @Test
    public void testCrearCliente() throws Exception {
        TipoDocumento tipoDocumento = TipoDocumento.C;
        String numeroDocumento = "23445322";
        String primerNombre = "Jose";
        String primerApellido = "Fernandez";
        String segundoApellido = "Garces";
        String telefono = "123456";

        ClienteRequest clienteRequest = new ClienteRequest();
        clienteRequest.setTipoDocumento(tipoDocumento);
        clienteRequest.setNumeroDocumento(numeroDocumento);
        clienteRequest.setPrimerNombre(primerNombre);
        clienteRequest.setPrimerApellido(primerApellido);
        clienteRequest.setSegundoApellido(segundoApellido);
        clienteRequest.setTelefono(telefono);

        ResultActions resultActionsPost = mockMvc.perform(MockMvcRequestBuilders
                .post("/cliente")
                .content(objectMapper.writeValueAsString(clienteRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());//200

        String responseContent = resultActionsPost.andReturn().getResponse().getContentAsString();
        JsonNode jsonNode = objectMapper.readTree(responseContent);

        assertTrue(jsonNode.has("primerNombre"));
        assertTrue(jsonNode.has("segundoNombre"));
        assertTrue(jsonNode.has("primerApellido"));
        assertTrue(jsonNode.has("segundoApellido"));
        assertTrue(jsonNode.has("telefono"));
        assertTrue(jsonNode.has("direccion"));
        assertTrue(jsonNode.has("ciudadResidencia"));

        mockMvc.perform(MockMvcRequestBuilders
                .get("/cliente/buscar")
                .param("tipoDocumento", tipoDocumento.name())
                .param("numeroDocumento", numeroDocumento)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());//200

    }

}
